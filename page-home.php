<?php /* Template Name: Home */ ?>
<?php
/**
 * The template for displaying the homepage.
 *
 * @package rd
 * 
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>

<article class="para">
	<?php while ( have_posts() ) : the_post(); ?>
		<section class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<?php get_template_part( 'template-parts/content', 'page' ); ?>
				</div>
				<?php if( have_rows('benefits') ): ?>
					<?php while( have_rows('benefits') ): the_row(); ?>
						<div class="col-sm-6 col-md-3">
							<?php if(get_sub_field('image')):?><img src="<?php the_sub_field('image'); ?>" style="height: 44px; width: auto;" alt="Benefits of Junk 4 Good"><?php endif;?>
							<?php the_sub_field('blurb'); ?>
							<hr>
							<?php if( have_rows('benefit_list') ): ?>
								<ul class="list-unstyled">
								<?php while( have_rows('benefit_list') ): the_row(); ?>
									<li><img src="<?php echo get_template_directory_uri();?>/assets/svg/tick.01.svg" style="width: 30px;margin: 4px;" alt="Checkmark">
										<?php the_sub_field('label'); ?>
									</li>
								<?php endwhile; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>
		<section class="feature">
			<section class="container">
				<div class="row">
					<div class="col-sm-12 col-md-5">
						<?php if( get_field('how_it_works') ): ?>
							<?php 
								echo get_field('how_it_works'); 
							?>
						<?php endif; ?>
					</div>
					<div class="clearfix visible-sm-block"><hr></div>
					<div class="col-sm-12 col-md-7 text-center">
						
						<div class="row">
						
						<?php if( have_rows('steps') ): 
							while(have_rows('steps')): the_row();?>
							
							<div class="col-sm-4">
								<div class="icon-block">
									<img src="<?php the_sub_field('step_image'); ?>" style="max-height: 140px;" alt="How to get started step">
								</div>
								<?php the_sub_field('step_text'); ?>
							</div>
							
							<?php endwhile; endif;?>
						</div>	
		
					</div>
				</div>
			</section>
		</section>
		<section class="container">
			<?php if( have_rows('features') ): 
				$cnt = 1;
				while(have_rows('features')): the_row();
					// skip the first feature (it's above)
					// create the zigzag layout for remainings features
					if ( $cnt > 0) {	
				?>
				<div class="row">
					<div class="col-md-<?php echo (($cnt % 2) == 1 ? 5 : 7); ?>">
				<?php
					echo get_sub_field('content_left');
				?>
					</div>
					<div class="col-md-<?php echo (($cnt % 2) == 1 ? 7 : 5); ?>">
				<?php
					echo get_sub_field('content_right');
				?>
					</div>
				</div>
				<hr>
				<?php
					}
				$cnt++;
				endwhile;
			endif; ?>
			
			<?php $randomquote = get_randomquote(); 
			
			if (!empty($randomquote)): ?>

				<div class="row">
					<div class="col-md-5">
						
						<?php $randomquote = get_randomquote(); ?>
						
						<h3><?php echo $randomquote['persons_name'];?></h3>
						<h4><?php echo $randomquote['location_business'];?></h4>
						<p><?php echo $randomquote['background_info'];?></p>
					</div>
					<div class="col-md-7">
						<h3>Here's What <?php echo $randomquote['persons_first_name'];?> Says About Us...</h3>
						<h2>&ldquo;<?php echo $randomquote['quote'];?>&rdquo;</h2>
						<p><a href="<?php echo get_permalink(get_page_by_title('Testimonials'));?>">More Testimonials?</a> | <a href="/testimonials/your-testimonial/">Tell Us Yours!</a></p>
					</div>
				</div>

			<?php endif; ?>
			
		</section>

	<?php endwhile; // End of the loop. ?>
	
	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
</article>


<?php get_footer(); ?>
