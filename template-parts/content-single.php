<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

?>



<?php the_title( '<h1>', '</h1>' ); ?>

<?php ADDTOANY_SHARE_SAVE_KIT() ?>
<hr>
 
<?php 
	
$thumb_id = get_post_thumbnail_id($post->ID);
$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);

?>


<?php the_post_thumbnail("large", array('alt' => $alt ) ); ?> 

<?php the_content(); ?>

<?php
	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redemptive-developments' ),
		'after'  => '</div>',
	) );
?>
<hr>
<div class="entry-meta">
	<?php redemptive_developments_posted_on(); ?>
</div><!-- .entry-meta -->
	



