<?php
/**
 * Template Name: Services Overview 
 * The template for displaying services.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>
	
	
 <article class="para">
		<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>
		
		
		<?php if ( have_posts() ) : the_post(); ?>
		
			<div class="row">
				<div class="col-md-12">
					<h1><?php echo $post->post_title ?></h1>
					
					<?php if(get_field('sub_title_text')):?>
						<?php echo '<h2>'.get_field('sub_title_text').'</h2>'?>
					<?php endif; ?>
					
					<?php the_content(); ?>
					<hr>
				</div>
			</div>
	
		<?php endif ?>
		
		<?php $rows = get_field('service_offerings');
		
		if($rows){
		
			foreach($rows as $row){ ?>
			
			<div class="row">
				<div class="col-md-4 hidden-xs hidden-sm">
					<?php echo '<img src="'. $row['service_image']['url'] .'" class="img-responsive" />'; ?>
				</div>
				<div class="col-md-8 col-sm-12">
					<h4><?php echo $row['service_name']; ?></h4>
					<p><?php echo $row['short_description']; ?></p>
					<div class="btn-group">
						<a class="btn btn-default" href="<?php echo $row['button_url'];?>">
								<?php echo $row['button_text']; ?>
						</a>
					</div>
				</div>
			</div>		
			<hr>
			<?php } // end for
			} //end if ?>
		
	</section>

	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
	
</article>

<?php get_footer(); ?>


	







