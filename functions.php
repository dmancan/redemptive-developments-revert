<?php
/**
 * rd functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package rd
 */

require get_template_directory() . '/inc/core.php';
require get_template_directory() . '/inc/functions.php';
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
require get_template_directory() . '/inc/wp_bootstrap_breadcrumbs.php';

