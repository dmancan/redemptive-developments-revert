<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>

<article class="para">
	<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>
		
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="col-md-6">
					<?php the_title( '<h1>', '</h1>' ); ?>
					
					<?php if(get_field('sub_title_text')):?>
						<?php echo '<h2>'.get_field('sub_title_text').'</h2>'?>
					<?php endif; ?>
					
					<?php ADDTOANY_SHARE_SAVE_KIT() ?>
					<hr>
					
					<?php get_template_part( 'template-parts/content', 'page' ); ?>
					
					
				</div>
				<div class="col-md-6">
					
					
					<?php if (get_field( 'form_snippet' )):?>
					
					<div class="well">
						<div class="row">
							<div class="col-xs-3">
								<img src="<?php echo get_template_directory_uri();?>/assets/svg/headset.01.svg" alt="Book Now!">
							</div>
							<div class="col-xs-9">	
								<h3><?php if(get_field('contact_cta')):?> <?php the_field('contact_cta');?> <?php endif;?></h3>
								<h4><?php if(get_field('secondary_cta')):?> <?php the_field('secondary_cta');?> <?php endif;?></h4>
								
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-xs-12">
								<?php the_field('form_snippet');?>
							</div><!--end col-->
						</div><!--end row-->
					</div><!--end well-->
					
				<?php endif;?>
					
					
					<?php get_sidebar(); ?>
				</div><!--end md-6 col-->
			</div><!--end main row-->
			
		<?php endwhile; // End of the loop. ?>
		
	</section>
	
	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
	
</article>
<?php get_footer(); ?>
