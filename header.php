<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rd
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png">
<link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png">
<link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png" sizes="16x16">
<link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png" sizes="32x32">
<link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png" sizes="48x48">
<link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png" sizes="64x64">
<link rel="icon" href="<?php echo get_template_directory_uri()?>/assets/images/favicon.png" sizes="128x128">
	
<!--[if IE]>
<link rel="shortcut icon" href="favicon.ico"/>
<![endif]-->

<meta http-equiv="cleartype" content="on">

</head>

<body <?php body_class(); ?>>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KD8KTZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KD8KTZ');</script>
<!-- End Google Tag Manager -->


<div class="viewport">
	<main>

<!-- 
	################################################################################ 
	Navigation is here
	################################################################################
-->
	<section class="tagline-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-5 col-md-7 text-right">
					<h2 class="tagline">NON-PROFIT JUNK REMOVAL</h2><span><a href="tel:1-844-586-5446"> 1-844-JUNK-4-GOOD</a> | <a href="tel:780-761-9636">780-761-9636</a></span>
				</div>
			</div>
		</div>
	</section>

	<nav class="navbar navbar-default">
		<div class="container">
			
			<div class="navbar-header visible-xs visible-sm">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

	  			<a class="navbar-brand" href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri();?>/assets/svg/logo.02.svg" alt="Junk 4 Good Home">
	  			</a>
	  		</div>
			
			
			<div class="collapse navbar-collapse" id="navigation">
		  		<?php /* Primary navigation */
				wp_nav_menu( array(
				  'menu' => 'Primary',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
	  		</div>
	  		
	  		<div class="navbar-header visible-md visible-lg">
		  		<a class="navbar-brand  " href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri();?>/assets/svg/logo.01.svg" alt="Junk 4 Good Home">
		  		</a>
	  		</div>
	  		

		  		
	  		<div class="toolbar-wrapper hidden-xs">
			 	<div class="main-cta">
					<h2 class="price-check">CHECK PRICING & BOOKING</h2>
					 <a href="/pricing/" class="btn btn-primary btn-lg btn-wide">Check now</a>
			 	</div>
		  	</div>
	  		
		</div>
	</nav>
			