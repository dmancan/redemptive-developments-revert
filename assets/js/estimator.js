/* --------------------------------------------------

	framerate shim
		
-------------------------------------------------- */
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller
// fixes from Paul Irish and Tino Zijdel
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
/* --------------------------------------------------

	Estimator 
	
-------------------------------------------------- */
var Estimator = (function ($) {
    var dh = {};
    dh.config = {
	  	junk : ".estimator .junk",
	  	reveal : ".estimator .reveal",
	  	box : ".estimator .box",
	  	slider : ".estimator .slider",
	  	sliderVal : 0
    };
    dh.init = function () {
	    // initial positioning
		$(dh.config.reveal).css({"width" : dh.config.sliderVal + "%"});
	  	$(dh.config.box).css({"width" : dh.config.sliderVal + "%"});
	  	dh.ui.loop();
    };
  	dh.ui = {
  		update : function () {
	  		// Find the slider val
			dh.config.sliderVal = parseInt($(dh.config.slider).val());
			// For Troubleshooting: Update the h1 so I can see the value;
  			//  $('h1').html(dh.config.sliderVal);
  			
  			if(dh.config.sliderVal < 20){
  				$(dh.config.box).css("width" , "65%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk1.png');
  			}else if((dh.config.sliderVal >= 20)&&(dh.config.sliderVal < 35)){
	  			$(dh.config.box).css("width" , "69%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk2.png');
  			}else if((dh.config.sliderVal >= 35)&&(dh.config.sliderVal < 50)){
	  			$(dh.config.box).css("width" , "72%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk3.png');
  			}else if((dh.config.sliderVal >= 50)&&(dh.config.sliderVal < 65)){
	  			$(dh.config.box).css("width" , "75%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk4.png');
  			}else if((dh.config.sliderVal >= 65)&&(dh.config.sliderVal < 80)){
	  			$(dh.config.box).css("width" , "80%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk5.png');
  			}else if((dh.config.sliderVal >= 80)&&(dh.config.sliderVal < 85)){
	  			$(dh.config.box).css("width" , "90%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk6.png');
  			}else if(dh.config.sliderVal >= 85){
	  			$(dh.config.box).css("width" , "100%");
	  			$(dh.config.junk).attr('src','/wp-content/themes/redemptive-developments-revert/assets/images/junk7.png');
	  			}
	  	},
     	loop : function () {
	     	// slider moved? update display...
	     	dh.ui.update();
        	requestAnimationFrame(dh.ui.loop);
    	}
  	};
    return dh;
})(jQuery);
