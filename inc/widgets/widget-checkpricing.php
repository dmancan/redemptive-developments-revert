<?php
/*
Plugin Name: RD Book Online
Plugin URI: http://ethicalux.com
Description: Check pricing widget
Author: EUX
Version: 1.0
Author URI: http://ethicalux.com
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'Book_Online' );
});	

/**
 * Adds My_Widget widget.
 */
class Book_Online extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Book_Online', // Base ID
			__('Check Pricing', 'check_pricing_domain'), // Name
			array( 'description' => __( 'Book online form', 'check_pricing_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
     	echo $args['before_widget'];
     	if ( ! empty( $instance['title'] ) ) {
			//echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		/********************************************************************************/
		?>
			<div class="well cta">
				<div class="text-center">
					<img src="<?php echo get_template_directory_uri() ?>/assets/svg/headset.01.svg" style="display: inline-block; max-width: 66px; margin: 4px;">
					<h3 style="margin: 11px 0 0 0;">
					<?php
						if ( ! empty( $instance['title'] ) ) {
							echo apply_filters( 'widget_title', $instance['title'] );
						}
					?>
					</h3>
					<p>No credit card required!</p>
				</div>
				

				
				
				<div class="vonigo-form">
					<form class="form-horizontal" action="http://redemptivedevelopments.vonigo.com/external/" accept-charset="UTF-8" method="get" onsubmit="return sbm(this)">
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" class="form-control" id="zip" placeholder="Enter Postal Code" maxlength="7">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="hidden" id="clienttypeid" name="clienttypeid" value="1" />
								<input type="hidden" id="servicetypeid" name="servicetypeid" value="11" />
								<button type="submit" class="btn btn-default btn-block">Book Now!</button>
								<input type="hidden" id="promo" name="promo" value="" />
							</div>
						</div>
					</form>
				 </div>
				 
				 
				 <div class="text-center">
                      <h4>Or Call Us<br>
<a href="tel:1-844-586-5446">1-844-586-5446</a> <br> <a href="tel: 780-761-9636"> 780-761-9636</a></h4>
                 </div>
			</div>
		<?php
		/********************************************************************************/
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'check_pricing_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class
?>