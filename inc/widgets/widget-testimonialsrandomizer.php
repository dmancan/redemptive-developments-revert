<?php
/*
Plugin Name: EUX sub menu
Plugin URI: http://ethicalux.com
Description: display sub menu in sidebar
Author: EUX
Version: 1.0
Author URI: http://ethicalux.com
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'Testimonials_Randomizer' );
});	

/**
 * Adds My_Widget widget.
 */
class Testimonials_Randomizer extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Testimonials_Randomizer', // Base ID
			__('Testimonials Randomizer', 'testimonials_randomizer_domain'), // Name
			array( 'description' => __( 'Display random testimonial', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
     	echo $args['before_widget'];
		
		/********************************************************************************/
		?>
		
     	<blockquote>
     	
     	<?php 
     	
     	$rand_row = get_randomquote();
     	// This function is in the functions.php file
   
		echo '<h5>'. $rand_row['testimonial_title'] .'</h5>';
		echo '<span class="quote">'. $rand_row['quote'] . '</span>';
		echo '<footer>' .$rand_row['persons_name'] . ' ';
		echo '<cite title="Source Title">'. $rand_row['location_business'] .'</cite>';
		echo '</footer>';
		
		?>
			
		</blockquote>
		
		<?php
		/********************************************************************************/
		echo $args['after_widget'];
	}


	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'testimonials_randomizer_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class
?>