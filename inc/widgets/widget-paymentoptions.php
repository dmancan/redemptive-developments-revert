<?php
/*
Plugin Name: RD Payment Options
Plugin URI: http://ethicalux.com
Description: See the types of payment options
Author: EUX
Version: 1.0
Author URI: http://ethicalux.com
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'Payment_Options' );
});	

/**
 * Adds My_Widget widget.
 */
class Payment_Options extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Payment_Options', // Base ID
			__('Payment Options', 'check_payments_domain'), // Name
			array( 'description' => __( 'Payment Options Form', 'check_payments_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
     	echo $args['before_widget'];
     	if ( ! empty( $instance['title'] ) ) {
			//echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		/********************************************************************************/
		?>
			<div class="well payment">
				<h3 style="margin: 11px 0 0 0;">
					<?php
						if ( ! empty( $instance['title'] ) ) {
							echo apply_filters( 'widget_title', $instance['title'] );
						}
					?>
					</h3>
                         <p><?php
						if ( ! empty( $instance['description'] ) ) {
							echo apply_filters( 'description', $instance['description'] );
						}
					?></p>
                         <hr>
                        
                        <?php
						if ( ! empty( $instance['payment_options'] ) ) {
							echo apply_filters( 'payment_options', $instance['payment_options'] );
						}
					?>
                        
                         </div><!--end well-->
		<?php
		/********************************************************************************/
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'check_pricing_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
		if ( isset( $instance[ 'description' ] ) ) {
			$description = $instance[ 'description' ];
		}
		else {
			$description = __( 'New description', 'check_pricing_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:' ); ?></label> 
			<textarea row="20" class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo $description; ?></textarea>
		</p>
		
		<?php
		if ( isset( $instance[ 'payment_options' ] ) ) {
			$payment_options = $instance[ 'payment_options' ];
		}
		else {
			$payment_options = __( 'Payment Options List', 'check_pricing_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'payment_options' ); ?>"><?php _e( 'Payment Options' ); ?></label> 
			<textarea row="20" class="widefat" 
			id="<?php echo $this->get_field_id( 'payment_options' ); ?>" 
			name="<?php echo $this->get_field_name( 'payment_options' ); ?>">
			<?php echo $payment_options; ?></textarea>
		</p>
		
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
		$instance['payment_options'] = ( ! empty( $new_instance['payment_options'] ) ) ? $new_instance['payment_options'] : '';
		return $instance;
	}

} // class
?>