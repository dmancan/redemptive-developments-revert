<?php
/*
Plugin Name: EUX sub menu
Plugin URI: http://ethicalux.com
Description: display sub menu in sidebar
Author: EUX
Version: 1.0
Author URI: http://ethicalux.com
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'Testimonials' );
});	

/**
 * Adds My_Widget widget.
 */
class Testimonials extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Testimonials', // Base ID
			__('Testimonials', 'text_domain'), // Name
			array( 'description' => __( 'Display random testimonial', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
     	echo $args['before_widget'];?>
     	
     	<blockquote>
     	
     	<?php
     	/********************************************************************************/
     	
     	if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		
		if ( ! empty( $instance['testimonial'] ) ) {
			echo apply_filters( 'widget_testimonial', $instance['testimonial'] );
		}
		if ( ! empty( $instance['cite'] ) ) {
			echo '<footer>' . apply_filters( 'widget_cite', $instance['cite'] ) . '</footer>';
		}
		?>
		</blockquote>
		<?php
		/********************************************************************************/
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
		if ( isset( $instance[ 'testimonial' ] ) ) {
			$testimonial = $instance[ 'testimonial' ];
		}
		else {
			$testimonial = __( 'New testimonial', 'text_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'testimonial' ); ?>"><?php _e( 'Testimonial:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'testimonial' ); ?>" name="<?php echo $this->get_field_name( 'testimonial' ); ?>" type="text" value="<?php echo esc_attr( $testimonial ); ?>">
		</p>
		<?php
		if ( isset( $instance[ 'cite' ] ) ) {
			$cite = $instance[ 'cite' ];
		}
		else {
			$cite = __( 'Customer', 'text_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'cite' ); ?>"><?php _e( 'Customer:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'cite' ); ?>" name="<?php echo $this->get_field_name( 'cite' ); ?>" type="text" value="<?php echo esc_attr( $cite ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['testimonial'] = ( ! empty( $new_instance['testimonial'] ) ) ? strip_tags( $new_instance['testimonial'] ) : '';
		$instance['cite'] = ( ! empty( $new_instance['cite'] ) ) ? strip_tags( $new_instance['cite'] ) : '';

		return $instance;
	}

} // class
?>