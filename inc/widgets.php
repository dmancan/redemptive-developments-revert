<?php
/**
 * widgets
 */
 
// unregister default widgets (we don't need 'em)
function unregister_default_widgets() {     
	unregister_widget('WP_Widget_Pages');     
	unregister_widget('WP_Widget_Calendar');     
	// unregister_widget('WP_Widget_Archives');     
	unregister_widget('WP_Widget_Links');     
	unregister_widget('WP_Widget_Meta');     
	unregister_widget('WP_Widget_Search');    
	// unregister_widget('WP_Widget_Text'); */  
	// unregister_widget('WP_Widget_Categories');  */   
	// unregister_widget('WP_Widget_Recent_Posts');     
	unregister_widget('WP_Widget_Recent_Comments');     
	unregister_widget('WP_Widget_RSS');    
	// unregister_widget('WP_Widget_Tag_Cloud');    
	unregister_widget('WP_Nav_Menu_Widget');     
	unregister_widget('Twenty_Eleven_Ephemera_Widget'); 
} 
add_action('widgets_init', 'unregister_default_widgets', 11);

// custom for RD
require get_template_directory() . '/inc/widgets/widget-testimonials.php';
require get_template_directory() . '/inc/widgets/widget-testimonialsrandomizer.php';
require get_template_directory() . '/inc/widgets/widget-checkpricing.php';
require get_template_directory() . '/inc/widgets/widget-submenu.php';
require get_template_directory() . '/inc/widgets/widget-callout.php';
require get_template_directory() . '/inc/widgets/widget-paymentoptions.php';

?>