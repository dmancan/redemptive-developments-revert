<?php
/**
 * EUX
 *
 * Custom Post Type example 
 * "Home Page"
 */

/*
add_action( 'init', 'create_home_page' );
function create_home_page() {
	 $labels = array(
    'name' => _x('Home', 'post type general name'),
    'singular_name' => _x('Home', 'post type singular name'),
    'add_new' => _x('Add New', 'Home Page'),
    'add_new_item' => __('Add new Home Page'),
    'edit_item' => __('Edit Home Page'),
    'new_item' => __('New Home Page'),
    'view_item' => __('View Home Page'),
    'search_items' => __('Search Home Pages'),
    'not_found' =>  __('No Home Pages found'),
    'not_found_in_trash' => __('No Home Pages found in Trash'),
    'parent_item_colon' => ''
  );

  $supports = array('title',  'page-attributes');

  register_post_type( 'Home Page',
    array(
      'labels' => $labels,
      'public' => true,
      'supports' => $supports,
      'hierarchical' => true
    )
  );
}
*/

add_action( 'init', 'create_home_page' );

function create_home_page() {

	register_post_type( 'Home Page', array(
	  'labels' => array(
	    'name' => 'Home Pages',
	    'singular_name' => 'Home Page',
	   ),
	  'description' => 'Home pages for this site.',
	  'public' => true,
	  'menu_position' => 20,
	  'supports' => array( 'title', 'editor', 'page-attributes', 'custom-fields' )
	));
	//comment this out if this page isn't showing up in the wp admin menu
	//flush_rewrite_rules();
}

function custom_home_loop( $query ) { 
	if ( is_home() && $query->is_main_query() ) 
		$query->set( 'post_type', array( 'post', 'Home Page') ); 
		return $query; 
} 
add_filter( 'pre_get_posts', 'custom_home_loop' );
	
?>