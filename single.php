<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rd
 */

get_header(); ?>
<article class="para">
	<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>
		
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="col-md-8">

					<?php get_template_part( 'template-parts/content', 'single' ); ?>

					<?php //the_post_navigation(); ?>
				</div>
				<div class="col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>

		<?php endwhile; // End of the loop. ?>
		
	</section>

	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>

</article>

<?php get_footer(); ?>
